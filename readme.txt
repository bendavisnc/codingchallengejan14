README
---------------------------------------------------------
Phase 2
Your client has asked you to make this library available via an API.  Your API should
implement a single endpoint called "/mmmr" with no query string parameters.  When a POST
request is called to "/mmmr", a single JSON object will be passed with an attribute called
"numbers".  "numbers" is a JSON array of n numbers that should be processed.  The script should process the mean, median, mode and range of the numbers and return back a
JSON object.  

If any value does not exist, an empty string should be returned.
Any system errors should be handled gracefully with a JSON response and a 500 error.  
If a request is sent to "/mmmr" that are is not a POST request, a JSON response and a 404 error should be returned.
---------------------------------------------------------
 
Included you'll find 2 php files:
index.php - my actual implementation of the coding challenge.
test.php  - the additional code that I wrote to test everything.

My friend James told me that he thought I'd be a good fit with you guys and your company.
So he sent me this coding challenge and the one for front end development.

I'm honestly somewhat new to PHP and admittingly still have a lot to learn, but I
hope that I've shown that I understand the core concepts of the language, and more 
importantly, my use of object oriented practices when solving this problem. 


CONTACT
Ben Davis
252 578 9587
bendavisnc.tumblr.com