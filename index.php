		<?php
// 			error_reporting(~0); ini_set('display_errors', 1);
			
			class AverageStats {
				
				const DECIMAL_PRECISION = 3;
				
				function __construct($numbers) {
					$this->mean = $this->calculateMean($numbers);
					$this->median = $this->calculateMedian($numbers);
					$this->mode = $this->calculateMode($numbers);
					$this->range = $this->calculateRange($numbers);
				}
				
				function calculateMean($numbers) {
					$sum = 0;
					foreach($numbers as $number) {
						$sum += $number;
					}
					return round($sum / sizeof($numbers), AverageStats::DECIMAL_PRECISION);
				}
				
				function calculateMedian($numbers) {
					sort($numbers);
					// if we have an odd set, return the middle element in the set
					if (sizeof($numbers) % 2 == 1) {
						return $numbers[sizeof($numbers) / 2];
					}
					// else return the mean of the middle two items in the set
					else {
						return $this->calculateMean(array($numbers[sizeof($numbers) / 2 - 1], $numbers[sizeof($numbers) / 2]));
					}
				}
				
				function calculateMode($numbers) {
					// create a hash map in the format <number value : number occurences count>
					$mapForCounting = array();
					foreach($numbers as $number) {
						if (array_key_exists($number, $mapForCounting)) {
							$mapForCounting[$number] += 1;
						}
						else {
							$mapForCounting[$number] = 1;
						}
					}
					asort($mapForCounting);
					// find which number value has the most occurences. if they all have the same, then return an empty string
					$mostOccurences = 1;
					$mostOccurencesVal = "";
					foreach ($mapForCounting as $key => $value) {
						if ($value > $mostOccurences) {
							$mostOccurences = $value;
							$mostOccurencesVal = $key;
						}
					}
					return $mostOccurencesVal;
				}
				
				function calculateRange($numbers) {
					// if we don't have at least 2 numbers, we can't return a difference
					if (sizeof($numbers) < 2) {
						return "";
					}
					else {
						sort($numbers);
						return abs($numbers[0] - $numbers[sizeof($numbers) - 1]);
					}
					
				}
				
				function generateResults() {
					$resultsArray =  array("results" => array("mean" => $this->mean, "median" => $this->median, "mode" => $this->mode,  "range" => $this->range));
					return $resultsArray;
				}
			}
			// make sure that we've received a POST request
			$whichRequestType = $_SERVER['REQUEST_METHOD'];
			if ($whichRequestType == 'POST') {
				try {
					$jsonStringIn = file_get_contents('php://input');
					$jsonStringInDecoded = json_decode($jsonStringIn, TRUE);
					if (empty($jsonStringInDecoded["numbers"])) {
						throw new Exception("No 'numbers' set specified.");
					}
					$averageStats = new AverageStats($jsonStringInDecoded["numbers"]);
					header('Content-type: application/json');
					echo json_encode($averageStats->generateResults(), TRUE);
				} catch(Exception $e) {
					// send an empty json response and a 505 error if anything unexpected happens
					header('HTTP/1.1 500 Internal Server Error');
					echo json_encode("", TRUE);
				}
			}
			else {
				// if the request is anything other than post, return a 404 error
				header('HTTP/1.0 404 Not Found');
				echo json_encode("", TRUE);
			}
			
		?>